// Select color input
const color = document.querySelector('#colorPicker'); // color without its value to be adabtable to  the user change after coloring some cells
// Select size input
const sizePicker = document.querySelector('#sizePicker');
const height = document.querySelector('#inputHeight');
const width = document.querySelector('#inputWidth');
const pixelCanvas = document.querySelector('#pixelCanvas');
// When size is submitted by the user, call makeGrid()
sizePicker.addEventListener('submit', e => {
  e.preventDefault();
  makeGrid();
});
function makeGrid() {
// to reset the pixelCanvas on the next call
  while (pixelCanvas.firstChild) {
    pixelCanvas.removeChild(pixelCanvas.firstChild);
  }
// iteration for table rows
  for (let i = 1; i <= height.value; i++) {

    let tr = document.createElement('tr');
    pixelCanvas.appendChild(tr);
// iteration for row's cells
    for (let j = 1; j <= width.value; j++) {

      let cell = document.createElement('td');
      tr.appendChild(cell);
// styling the cell with the selected color 
      cell.addEventListener('mousedown', e => {
        e.target.style.backgroundColor = color.value;
       creatTooltip(cell);
      });

// additional feature( to remove the color on double Click)
      cell.addEventListener('dblclick', e => {
        e.target.style.backgroundColor = null;
        cell.innerHTML = '';
      });

      cell.addEventListener('mouseover', e => {
        if (e.target.style.backgroundColor) {
         creatTooltip(cell);
        }
      });
    }
  }
}
// function to creat the span and fire the tooltip
function creatTooltip(cell){
  let span = document.createElement('span');
  span.innerText = "Double click to remove the color";
  span.className = 'tooltip';
  cell.appendChild(span);
}